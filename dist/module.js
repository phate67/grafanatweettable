'use strict';

System.register(['lodash', './tweettable_ctrl'], function (_export, _context) {
  "use strict";

  var _, TweetTableCtrl;

  return {
    setters: [function (_lodash) {
      _ = _lodash.default;
    }, function (_tweettable_ctrl) {
      TweetTableCtrl = _tweettable_ctrl.TweetTableCtrl;
    }],
    execute: function () {
      _export('PanelCtrl', TweetTableCtrl);
    }
  };
});
//# sourceMappingURL=module.js.map

'use strict';

System.register(['angular', 'lodash', 'jquery', 'moment', 'app/core/utils/file_export', 'app/plugins/sdk', './transformers', 'app/core/time_series', './rendering'], function (_export, _context) {
  "use strict";

  var angular, _, $, moment, FileExport, MetricsPanelCtrl, transformDataToTable, TimeSeries, rendering, _createClass, _get, TweetTableCtrl;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_angular) {
      angular = _angular.default;
    }, function (_lodash) {
      _ = _lodash.default;
    }, function (_jquery) {
      $ = _jquery.default;
    }, function (_moment) {
      moment = _moment.default;
    }, function (_appCoreUtilsFile_export) {
      FileExport = _appCoreUtilsFile_export;
    }, function (_appPluginsSdk) {
      MetricsPanelCtrl = _appPluginsSdk.MetricsPanelCtrl;
    }, function (_transformers) {
      transformDataToTable = _transformers.transformDataToTable;
    }, function (_appCoreTime_series) {
      TimeSeries = _appCoreTime_series.default;
    }, function (_rendering) {
      rendering = _rendering.default;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _get = function get(object, property, receiver) {
        if (object === null) object = Function.prototype;
        var desc = Object.getOwnPropertyDescriptor(object, property);

        if (desc === undefined) {
          var parent = Object.getPrototypeOf(object);

          if (parent === null) {
            return undefined;
          } else {
            return get(parent, property, receiver);
          }
        } else if ("value" in desc) {
          return desc.value;
        } else {
          var getter = desc.get;

          if (getter === undefined) {
            return undefined;
          }

          return getter.call(receiver);
        }
      };

      _export('TweetTableCtrl', TweetTableCtrl = function (_MetricsPanelCtrl) {
        _inherits(TweetTableCtrl, _MetricsPanelCtrl);

        //pageIndex: number;
        //dataRaw: any;
        //table: any;

        /** @ngInject */
        function TweetTableCtrl($scope, $injector, $rootScope) {
          _classCallCheck(this, TweetTableCtrl);

          var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(TweetTableCtrl).call(this, $scope, $injector));

          _this.$rootScope = $rootScope;

          var panelDefaults = {
            targets: [{}],
            transform: 'timeseries_to_columns',
            pageSize: null,
            showHeader: true,
            styles: [{
              type: 'date',
              pattern: 'Time',
              dateFormat: 'YYYY-MM-DD HH:mm:ss'
            }, {
              unit: 'short',
              type: 'number',
              decimals: 2,
              colors: ["rgba(245, 54, 54, 0.9)", "rgba(237, 129, 40, 0.89)", "rgba(50, 172, 45, 0.97)"],
              colorMode: null,
              pattern: '/.*/',
              thresholds: []
            }],
            columns: [],
            scroll: true,
            fontSize: '100%',
            sort: { col: 0, desc: true }
          };

          _this.pageIndex = 0;

          if (_this.panel.styles === void 0) {
            _this.panel.styles = _this.panel.columns;
            _this.panel.columns = _this.panel.fields;
            delete _this.panel.columns;
            delete _this.panel.fields;
          }

          _.defaults(_this.panel, panelDefaults);

          _this.events.on('data-received', _this.onDataReceived.bind(_this));
          _this.events.on('data-error', _this.onDataError.bind(_this));
          _this.events.on('data-snapshot-load', _this.onDataSnapshotLoad.bind(_this));
          _this.events.on('init-edit-mode', _this.onInitEditMode.bind(_this));
          _this.events.on('init-panel-actions', _this.onInitPanelActions.bind(_this));
          return _this;
        }

        _createClass(TweetTableCtrl, [{
          key: 'onInitEditMode',
          value: function onInitEditMode() {}
        }, {
          key: 'onInitPanelActions',
          value: function onInitPanelActions(actions) {}
        }, {
          key: 'issueQueries',
          value: function issueQueries(datasource) {
            var _this2 = this;

            this.pageIndex = 0;

            if (this.panel.transform === 'annotations') {
              return this.annotationsSrv.getAnnotations(this.dashboard).then(function (annotations) {
                _this2.dataRaw = annotations;
                _this2.render();
              });
            }

            return _get(Object.getPrototypeOf(TweetTableCtrl.prototype), 'issueQueries', this).call(this, datasource);
          }
        }, {
          key: 'onDataSnapshotLoad',
          value: function onDataSnapshotLoad(data) {
            this.onDataReceived(data.data);
          }
        }, {
          key: 'onDataError',
          value: function onDataError(err) {
            this.dataRaw = [];
            this.render();
          }
        }, {
          key: 'onDataReceived',
          value: function onDataReceived(dataList) {
            this.dataRaw = dataList;
            this.pageIndex = 0;

            //this.series = dataList.map(this.seriesHandler.bind(this));
            //this.data = this.parseSeries(this.series);

            // automatically correct transform mode based on data
            if (this.dataRaw && this.dataRaw.length) {
              if (this.dataRaw[0].type === 'table') {
                this.panel.transform = 'table';
              } else {
                if (this.dataRaw[0].type === 'docs') {
                  this.panel.transform = 'json';
                } else {
                  if (this.panel.transform === 'table' || this.panel.transform === 'json') {
                    this.panel.transform = 'timeseries_to_rows';
                  }
                }
              }
            }

            this.table = transformDataToTable(this.dataRaw, this.panel);
            this.table.sort(this.panel.sort);
            this.render(this.table);
          }
        }, {
          key: 'toggleColumnSort',
          value: function toggleColumnSort(col, colIndex) {
            if (this.panel.sort.col === colIndex) {
              if (this.panel.sort.desc) {
                this.panel.sort.desc = false;
              } else {
                this.panel.sort.col = null;
              }
            } else {
              this.panel.sort.col = colIndex;
              this.panel.sort.desc = true;
            }

            this.render();
          }
        }, {
          key: 'exportCsv',
          value: function exportCsv() {
            FileExport.exportTableDataToCsv(this.table);
          }
        }, {
          key: 'parseSeries',
          value: function parseSeries(series) {
            var _this3 = this;

            return _.map(this.series, function (serie, i) {
              return {
                label: serie.alias,
                data: serie.stats[_this3.panel.valueName],
                color: _this3.panel.aliasColors[serie.alias] || _this3.$rootScope.colors[i]
              };
            });
          }
        }, {
          key: 'seriesHandler',
          value: function seriesHandler(seriesData) {
            var series = new TimeSeries({
              datapoints: seriesData.datapoints,
              alias: seriesData.target
            });

            series.flotpairs = series.getFlotPairs(this.panel.nullPointMode);
            return series;
          }
        }, {
          key: 'link',
          value: function link(scope, elem, attrs, ctrl) {
            var data;
            var panel = ctrl.panel;
            var pageCount = 0;
            var formaters = [];

            function getTableHeight() {
              var panelHeight = ctrl.height;

              if (pageCount > 1) {
                panelHeight -= 26;
              }

              return panelHeight - 31 + 'px';
            }

            function appendTableRows(tbodyElem) {
              //var renderer = new TableRenderer(panel, data, ctrl.dashboard.timezone);
              tbodyElem.empty();
              //tbodyElem.html(renderer.render(ctrl.pageIndex));
              ctrl.data = data;
              ctrl.tbodyElem = tbodyElem;

              var tableRowsHtml = rendering(scope, elem, attrs, ctrl);
              //tbodyElem.html(tableRowsHtml);
            }

            function switchPage(e) {
              var el = $(e.currentTarget);
              ctrl.pageIndex = parseInt(el.text(), 10) - 1;
              renderPanel();
            }

            function appendPaginationControls(footerElem) {
              footerElem.empty();

              var pageSize = panel.pageSize || 100;
              pageCount = Math.ceil(data.rows.length / pageSize);
              if (pageCount === 1) {
                return;
              }

              var startPage = Math.max(ctrl.pageIndex - 3, 0);
              var endPage = Math.min(pageCount, startPage + 9);

              var paginationList = $('<ul></ul>');

              for (var i = startPage; i < endPage; i++) {
                var activeClass = i === ctrl.pageIndex ? 'active' : '';
                var pageLinkElem = $('<li><a class="table-panel-page-link pointer ' + activeClass + '">' + (i + 1) + '</a></li>');
                paginationList.append(pageLinkElem);
              }

              footerElem.append(paginationList);
            }

            function renderPanel() {
              var panelElem = elem.parents('.panel');
              var rootElem = elem.find('.table-panel-scroll');
              var tbodyElem = elem.find('tbody');
              var footerElem = elem.find('.table-panel-footer');

              /*
              var columns = [];
              for (var i = 0; i <  ctrl.table.columns.length; i++) {
                if (ctrl.table.columns[i].text ==='Time'|| ctrl.table.columns[i].text ==='tweets_term_stats.tweet_id'){
                  columns.push(ctrl.table.columns[i]);
                }
              }
               ctrl.table.columns = columns;
              */

              elem.css({ 'font-size': panel.fontSize });
              panelElem.addClass('table-panel-wrapper');

              appendTableRows(tbodyElem);
              appendPaginationControls(footerElem);

              rootElem.css({ 'max-height': panel.scroll ? getTableHeight() : '' });
            }

            elem.on('click', '.table-panel-page-link', switchPage);

            scope.$on('$destroy', function () {
              elem.off('click', '.table-panel-page-link');
            });

            ctrl.events.on('render', function (renderData) {
              data = renderData || data;
              if (data) {

                renderPanel();
              }
            });
          }
        }]);

        return TweetTableCtrl;
      }(MetricsPanelCtrl));

      _export('TweetTableCtrl', TweetTableCtrl);

      TweetTableCtrl.templateUrl = 'module.html';
    }
  };
});
//# sourceMappingURL=tweettable_ctrl.js.map

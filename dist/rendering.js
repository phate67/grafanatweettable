'use strict';

System.register(['lodash', 'moment', 'app/core/utils/kbn', 'angular'], function (_export, _context) {
  "use strict";

  var _, moment, kbn, angular, _typeof;

  function link(scope, elem, attrs, ctrl) {
    var data;
    var panel;
    var table;
    var timezone;
    var formaters = [];
    var colorState = {};

    elem = elem.find('.table-panel-table');

    var tableBodyHtml = render();
    ctrl.tbodyElem.html(tableBodyHtml);
    ctrl.renderingCompleted();

    /*
    ctrl.events.on('render', function() {
      var tableBodyHtml = render();
      ctrl.tbodyElem.html(tableBodyHtml);
      ctrl.renderingCompleted();
    });
    */

    function setElementHeight() {}

    function formatter(label, slice) {
      return "<div style='font-size:" + ctrl.panel.fontSize + ";text-align:center;padding:2px;color:" + slice.color + ";'>" + label + "<br/>" + Math.round(slice.percent) + "%</div>";
    }

    function addTable() {}

    function getColorForValue(value, style) {
      if (!style.thresholds) {
        return null;
      }

      for (var i = style.thresholds.length; i > 0; i--) {
        if (value >= style.thresholds[i - 1]) {
          return style.colors[i];
        }
      }
      return _.first(style.colors);
    }

    function defaultCellFormater(v) {
      if (v === null || v === void 0 || v === undefined) {
        return '';
      }

      if (_.isArray(v)) {
        v = v.join(',&nbsp;');
      }

      return v;
    }

    function createColumnFormater(style) {
      if (!style) {
        return this.defaultCellFormater;
      }

      if (style.type === 'date') {
        return function (v) {
          if (_.isArray(v)) {
            v = v[0];
          }
          var date = moment(v);
          if (timezone === 'utc') {
            date = date.utc();
          }
          return date.format(style.dateFormat);
        };
      }

      if (style.type === 'number') {
        var _ret = function () {
          var valueFormater = kbn.valueFormats[style.unit];

          return {
            v: function v(_v) {
              if (_v === null || _v === void 0) {
                return '-';
              }

              if (_.isString(_v)) {
                return _v;
              }

              if (style.colorMode) {
                colorState[style.colorMode] = getColorForValue(_v, style);
              }

              return valueFormater(_v, style.decimals, null);
            }
          };
        }();

        if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
      }

      return this.defaultCellFormater;
    }

    function formatColumnValue(colIndex, value) {
      if (formaters[colIndex]) {
        return formaters[colIndex](value);
      }

      for (var i = 0; i < panel.styles.length; i++) {
        var style = panel.styles[i];
        var column = table.columns[colIndex];
        var regex = kbn.stringToJsRegex(style.pattern);
        if (column.text.match(regex)) {
          formaters[colIndex] = createColumnFormater(style);
          return formaters[colIndex](value);
        }
      }

      formaters[colIndex] = this.defaultCellFormater;
      return formaters[colIndex](value);
    }

    function parseURL(text) {
      return text.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+/g, function (url) {
        var hyperlink = "<a href=\"" + url + "\" target=\"_blank\" style=\"color: rgb(0,0,255)\">" + url + "</a>";
        return hyperlink;
        //return url.link(url);
      });
    }

    function parseURL2(text) {
      var re = /<a \s*([^>]+)?>/g;
      var m;

      var annotatedText = text;
      while ((m = re.exec(text)) !== null) {
        var match = m[0];

        var href = m[1];

        var coloredReference = "<a " + href + "\" target=\"_blank\" style=\"color: rgb(0,0,255)\" >";

        annotatedText = annotatedText.replace(match, coloredReference);
      }

      return annotatedText;
    }

    function parseUsername(text) {
      var re = /([@]+(<em([^>]+)?>)?([A-Za-z0-9-_]+)(<\/em>)?)/g;
      var m;

      var annotatedText = text;
      while ((m = re.exec(text)) !== null) {
        var match = m[0];

        var usertag = m[1];
        var styleInfo = m[2];
        var username = m[4];

        var userUrl = "<a href=\"https://twitter.com/" + username + "\" target=\"_blank\" style=\"color: rgb(0,0,255)\">" + match + "</a>";
        if (styleInfo !== undefined) {
          var newStyle = "<em style=\"background-color:yellow; color: blue ; font-weight:bold\">";
          userUrl = userUrl.replace(styleInfo, newStyle);
          console.log(userUrl);
        }

        annotatedText = annotatedText.replace(match, userUrl);
      }

      return annotatedText;
    }

    function addStyleToKeyword(text) {

      return text.replace(/<em>/g, function (t) {
        return t.replace("<em>", "<em style=\"background-color:lightyellow; color: maroon; font-weight:bold\">");
      });
    }

    function parseHashtag(text) {
      var re = /([#]+(<em([^>]+)?>)?([A-Za-z0-9-_]+)(<\/em>)?)/g;
      var m;

      var annotatedText = text;
      while ((m = re.exec(text)) !== null) {
        var match = m[0];

        var hashTag = m[1];
        var styleInfo = m[2];
        var tagVal = m[4];

        var hashTagUrl = "<a href=\"https://twitter.com/hashtag/" + tagVal + "?src=hash\" target=\"_blank\" style=\"color: rgb(0,0,255)\">" + match + "</a>";
        if (styleInfo !== undefined) {
          var newStyle = "<em style=\"background-color:yellow; color: blue ; font-weight:bold\">";
          hashTagUrl = hashTagUrl.replace(styleInfo, newStyle);
          console.log(hashTagUrl);
        }

        annotatedText = annotatedText.replace(match, hashTagUrl);
      }

      return annotatedText;
    }

    function renderCell(columnIndex, value) {
      var addWidthHack = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];

      value = formatColumnValue(columnIndex, value);
      //value = _.escape(value);
      var style = '';
      if (colorState.cell) {
        style = ' style="background-color:' + colorState.cell + ';color: white"';
        colorState.cell = null;
      } else if (colorState.value) {
        style = ' style="color:' + colorState.value + '"';
        colorState.value = null;
      }

      var cellContent = value;
      //if ( table.columns[columnIndex].text ==='tweets_term_stats.tweet_id'){
      if (table.columns[columnIndex].text === 'Tweet Id') {
        var url = 'http://twitter.com/anyuser/status/' + value;
        cellContent = '<a href=\"' + url + '\" target=\"_blank\ style="color: rgb(0,0,255)""><font color="0000FF">' + value + '</a>';
      } else if (table.columns[columnIndex].text === 'Tweet') {

        cellContent = addStyleToKeyword(cellContent);
        cellContent = parseURL2(cellContent);
        cellContent = parseUsername(cellContent);
        cellContent = parseHashtag(cellContent);
      } else if (table.columns[columnIndex].text === 'Sentiment') {
        if (value === 'Positive') {
          cellContent = '<span style="background-color: #7EB26D"><b><font color="white"> ' + value + ' </font></b></span>';
        } else if (value === 'Negative') {
          cellContent = '<span style="background-color: #BF1B00"><b><font color="white"> ' + value + ' </font></b></span>';
        } else if (value === 'Neutral') {
          cellContent = '<span style="background-color: #F4D598"><b><font color="black"> ' + value + ' </font></b></span>';
        }
      }

      // because of the fixed table headers css only solution
      // there is an issue if header cell is wider the cell
      // this hack adds header content to cell (not visible)
      var widthHack = '';
      if (addWidthHack) {
        widthHack = '<div class="table-panel-width-hack">' + table.columns[columnIndex].text + '</div>';
      }

      return '<td' + style + '>' + cellContent + widthHack + '</td>';
    }

    function renderTable(page, panel, table) {
      var pageSize = panel.pageSize || 100;
      var startPos = page * pageSize;
      var endPos = Math.min(startPos + pageSize, table.rows.length);
      var html = "";

      for (var y = startPos; y < endPos; y++) {
        var row = table.rows[y];
        var cellHtml = '';
        var rowStyle = '';
        for (var i = 0; i < table.columns.length; i++) {
          cellHtml += renderCell(i, row[i], y === startPos);
        }

        if (colorState !== undefined) {
          if (colorState.row) {
            rowStyle = ' style="background-color:' + colorState.row + ';color: white"';
            colorState.row = null;
          }
        }

        html += '<tr ' + rowStyle + '>' + cellHtml + '</tr>';
      }

      return html;
    }

    function render() {
      if (!ctrl.data) {
        return;
      }

      data = ctrl.data;
      panel = ctrl.panel;
      table = ctrl.data;
      timezone = ctrl.dashboard.timezone;

      var html = renderTable(ctrl.pageIndex, panel, data);
      return html;
    }
  }

  _export('default', link);

  return {
    setters: [function (_lodash) {
      _ = _lodash.default;
    }, function (_moment) {
      moment = _moment.default;
    }, function (_appCoreUtilsKbn) {
      kbn = _appCoreUtilsKbn.default;
    }, function (_angular) {
      angular = _angular.default;
    }],
    execute: function () {
      _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
        return typeof obj;
      } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
      };
    }
  };
});
//# sourceMappingURL=rendering.js.map
